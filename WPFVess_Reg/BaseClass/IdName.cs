﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFVess_Reg.BaseClass
{
    public class IdName
    {
        public int ID { get; set; }
        public string VessName { get; set; }
        public string VessName_CN { get; set; }
    }
}
