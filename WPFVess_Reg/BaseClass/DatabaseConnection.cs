﻿//using Oracle.ManagedDataAccess.Client;



using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFVess_Reg.BaseClass
{
    public class DatabaseConnection
    {
        //private static string connString = "User Id=tos;Password=tos;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=58.40.126.174)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=cyora)))";
        //public static string connString = "User Id=TOS;Password=TOS;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=58.40.126.174)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=cyora)));";

        public static string connString = "User Id=TOS;Password=TOS;Data Source=CYORA;";

        public static OracleConnection Conn = new OracleConnection(connString);
        public static OracleCommand DeleteCommand { get; set; }
        public static OracleCommand InsertCommand { get; set; }
        public static OracleCommand SelectCommand { get; set; }
        public static OracleCommand UpdateCommand { get; set; }
        public static OracleDataAdapter DA_voyage { get; set; }
        public  DataSet DS_voyage { get; set; }
        public  DataTable DT_voyage { get; set; }
        public DatabaseConnection(string connecttext)
        {
            DA_voyage = new OracleDataAdapter();
            SelectCommand = new OracleCommand()
            {
                Connection = Conn,
                CommandType = CommandType.Text,
                CommandText = connecttext
            };
            Conn.Open();
            DT_voyage = new DataTable();
            DS_voyage = new DataSet();
            DA_voyage.SelectCommand = SelectCommand;
            DA_voyage.Fill(DS_voyage, "REG_voyage");
            DT_voyage = DS_voyage.Tables["REG_voyage"];
            Conn.Close();
        }

        public DatabaseConnection(string key,string connecttext)
        {
            try
            {
                if (key.ToUpper() == "DELETE")
                {
                    DeleteCommand = new OracleCommand()
                    {
                        Connection = Conn,
                        CommandType = CommandType.Text,
                        CommandText = connecttext
                    };
                    Conn.Open();
                    DA_voyage.DeleteCommand = DeleteCommand;
                    DeleteCommand.ExecuteNonQuery();
                    Conn.Close();
                }
                else if (key.ToUpper() == "INSERT")
                {
                    InsertCommand = new OracleCommand()
                    {
                        Connection = Conn,
                        CommandType = CommandType.Text,
                        CommandText = connecttext
                    };
                    Conn.Open();
                    DA_voyage.InsertCommand = InsertCommand;
                    InsertCommand.ExecuteNonQuery();
                    Conn.Close();
                }
                else if (key.ToUpper() == "UPDATE")
                {
                    UpdateCommand = new OracleCommand()
                    {
                        Connection = Conn,
                        CommandType = CommandType.Text,
                        CommandText = connecttext
                    };
                    Conn.Open();
                    DA_voyage.UpdateCommand = UpdateCommand;
                    UpdateCommand.ExecuteNonQuery();
                    Conn.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
