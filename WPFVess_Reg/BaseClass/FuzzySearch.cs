﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPFVess_Reg.BaseClass
{
    class FuzzySearch
    {
        public DataTable FSearch(DataTable DataTable ,string key)
        {
            DataTable DT = DataTable.Clone();
            DT.Clear();
            if(DataTable.Columns.Contains("all") !=true)
            {
                DataTable.Columns.Add("all");
            }
            try
            {
                int columnsCount = DataTable.Columns.Count;
                int rowsCount = DataTable.Rows.Count;
                //每一行数据平成一个单元格
                for(int i =0;i<rowsCount;i++)
                {
                    string rowValue = "";
                    for(int j=0;j<columnsCount;j++)
                    {
                        rowValue = rowValue + "," + DataTable.Rows[i][j];
                    }
                    DataTable.Rows[i]["all"] = rowValue;
                }

                if(key !="")
                {
                    DataRow[] dr = DataTable.Select("all like '%" + key + "%'");
                    for(int i=0;i<dr.Length;i++)
                    {
                        DT.ImportRow(dr[i]);
                    }
                }
                else
                {
                    DT = DataTable.Copy();
                }
                DataTable.Columns.Remove(DataTable.Columns["all"]);
                return DT;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            return DT;
        }
    }
}
