﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OracleClient;
using System.Data;

namespace WPFVess_Reg.BaseClass
{
    class DateGrid
    {
        public DateGrid()
        {
            Conn.ConnectionString = connString;
        }
        public const string connString = "User Id=tos;Password=tos;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.65.164.55)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=cyora)))";
        public const string selt = "SELECT   VESSEL.VESNAME, VESSEL.VESNAME_CN, VOYAGE.* FROM VESSEL RIGHT OUTER JOIN VOYAGE ON VESSEL.VES_ID = VOYAGE.VES_ID";
        public const string drop = "SELECT   VOYAGE.* FROM      VOYAGE";
        public const string name = "SELECT   VES_ID, VESNAME_CN, VESNAME FROM  VESSEL";
#pragma warning disable CS0618 // 类型或成员已过时
        public OracleConnection Conn { get; set; }
        public OracleCommand Mycommand { get; set; }
        public OracleCommand Drop_command { get; set; }
        public OracleCommand Name_command { get; set; }
        public OracleDataAdapter DA_voyage { get; set; }
        public DataSet DS_voyage { get; set; }
        public DataTable DT_voyage { get; set; }
        public DataTable Drop_dt { get; set; }
        public DataTable Name_dt{ get; set; }
        public int Count { get; set; }
#pragma warning restore CS0618 // 类型或成员已过时
    }
}
