﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace WPFVess_Reg.Converters
{
    public class VesNameToConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string vesName = values[0].ToString();
            DataTable table = values[1] as DataTable;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (vesName.ToString() == table.Rows[i]["VESNAME_CN"].ToString())
                {
                    return table.Rows[i]["VESNAME"].ToString();
                }
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
