﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using WPFVess_Reg.BaseClass;

namespace WPFVess_Reg.Converters
{
    public class NameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DatabaseConnection namelist = new DatabaseConnection("SELECT   VES_ID, VESNAME, VESNAME_CN FROM VESSEL");
            for (int i = 0; i < namelist.DT_voyage.Rows.Count; i++)
            {
                if (value.ToString() == namelist.DT_voyage.Rows[i]["VESNAME_CN"].ToString())
                {
                    return namelist.DT_voyage.Rows[i]["VESNAME"];
                }
            }
            return null;

        }



        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
