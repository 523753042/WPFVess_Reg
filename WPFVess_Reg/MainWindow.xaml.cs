﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFVess_Reg.BaseClass;
using WPFVess_Reg.ViewModel;

namespace WPFVess_Reg
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
 
        private  Vess_regViewModel vess_regViewModel = new Vess_regViewModel();
        public MainWindow()
        {
            InitializeComponent();

            DataContext = vess_regViewModel;
            vessColumn.ItemsSource = vess_regViewModel.MyNameConnection.DT_voyage.DefaultView;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


    }
}
