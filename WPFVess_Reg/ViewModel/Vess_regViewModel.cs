﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFVess_Reg.BaseClass;
using System.Windows;
using System.ComponentModel;

namespace WPFVess_Reg.ViewModel
{
    
    public class Vess_regViewModel : BaseViewModel
    {
        public Vess_regViewModel()
        {
            InsertRowCommand = new BaseCommand(InsertRow);
            DeleteRowCommand = new BaseCommand(DeleteRow);
            SaveCommand = new BaseCommand(Save);
        }
        private string textWord;
        public string TextWord
        {
            get
            {
                return textWord;
            }
            set
            {
                SetFilter(value);
                textWord = value.ToString();
            }
        }

        private int selectedIndex;
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                OnPropertyChanged("SelectedItem");
            }
        }
        private DatabaseConnection myconnection;
        /// <summary>
        /// 数据表
        /// </summary>
        public DatabaseConnection MyConnection
        {
            get
            {
                if(myconnection==null)
                {
                    myconnection = new DatabaseConnection("SELECT VOYAGE.*, VESSEL.VES_ID AS EXPR1, VESSEL.VESNAME, VESSEL.VESNAME_CN FROM  VOYAGE LEFT OUTER JOIN VESSEL ON VOYAGE.VES_ID = VESSEL.VES_ID");
                }
                return myconnection;
            }
            set { }
        }
        private DatabaseConnection mynameConnection;
        /// <summary>
        /// combobox列表
        /// </summary>
        public DatabaseConnection MyNameConnection
        {
            get
            {
                if (mynameConnection == null)
                {
                    mynameConnection = new DatabaseConnection("SELECT VES_ID, VESNAME_CN, VESNAME FROM VESSEL");
                }
                return mynameConnection;
            }
            set { }
        }


        public BaseCommand InsertRowCommand { get; set; }
        /// <summary>
        /// 插入行
        /// </summary>
        private void InsertRow()
        {
            DataRow newRow = MyConnection.DT_voyage.NewRow();
            if (SelectedIndex == 0)
            {
                MyConnection.DT_voyage.Rows.Add();
            }
            else
            {
                MyConnection.DT_voyage.Rows.InsertAt(newRow, SelectedIndex);
            }
        }

        public BaseCommand DeleteRowCommand { get; set; }
        /// <summary>
        /// 删除行
        /// </summary>
        private void DeleteRow()
        {
            if (MessageBox.Show("确定要删除，一旦删除无法恢复", "提示", MessageBoxButton.YesNoCancel) == MessageBoxResult.Yes)
            {
                string id = MyConnection.DT_voyage.Rows[SelectedIndex]["VOY_ID"].ToString();
                string sqlDelete = "delete from voyage where voy_id=" + id;
                MyConnection = new DatabaseConnection("delete", sqlDelete);
                MyConnection.DT_voyage.Rows[SelectedIndex].Delete();
                MyConnection.DT_voyage.AcceptChanges();
                OnPropertyChanged("MyConnection");
                MessageBox.Show("删除成功！", "提示");
            }
        }

        public BaseCommand SaveCommand { get; set; }
        /// <summary>
        /// 保存
        /// </summary>
        private void Save()
        {
            string ves_id = "";
            foreach (DataRow myrow in MyConnection.DT_voyage.Rows)
            {
                if (myrow.RowState == DataRowState.Added)
                {
                    string ves_name = myrow["VESNAME_CN"].ToString();
                    for (int i = 0; i < MyNameConnection.DT_voyage.Rows.Count; i++)
                    {
                        if (ves_name == MyNameConnection.DT_voyage.Rows[i]["VESNAME_CN"].ToString())
                        {
                            ves_id = MyNameConnection.DT_voyage.Rows[i]["VES_ID"].ToString();
                            break;
                        }
                    }
                    string voy_import = myrow["VOY_IMPORT"].ToString();
                    string voy_export = myrow["VOY_EXPORT"].ToString();
                    string rec_time = System.DateTime.Now.ToString();
                    string eta = myrow["ETA"].ToString();
                    string etb = myrow["ETB"].ToString();
                    string etd = myrow["ETD"].ToString();
                    string closing_time = myrow["CLOSING_TIME"].ToString();
                    string ata = myrow["ATA"].ToString();
                    string atb = myrow["ATB"].ToString();
                    string atd = myrow["ATD"].ToString();
                    string sqlInsert = "INSERT INTO VOYAGE (VES_ID, VOY_IMPORT, VOY_EXPORT,REC_TIME,ETA,ETB,ETD,CLOSING_TIME,ATA,ATB,ATD) " +
                            "VALUES (" + ves_id + ",'" + voy_import + "','" + voy_export + "'," +
                            ToDate(rec_time) + "," +
                            ToDate(eta) + "," +
                            ToDate(etb) + "," +
                            ToDate(etd) + "," +
                            ToDate(closing_time) + "," +
                            ToDate(ata) + "," +
                            ToDate(atb) + "," +
                            ToDate(atd) + ")";
                    MyConnection = new DatabaseConnection("insert", sqlInsert);
                }
                if (myrow.RowState == DataRowState.Modified)
                {
                    string ves_name = myrow["VESNAME_CN"].ToString();
                    for (int i = 0; i < MyNameConnection.DT_voyage.Rows.Count; i++)
                    {
                        if (ves_name == MyNameConnection.DT_voyage.Rows[i]["VESNAME_CN"].ToString())
                        {
                            ves_id = MyNameConnection.DT_voyage.Rows[i]["VES_ID"].ToString();
                            break;
                        }
                    }
                    string voy_import = myrow["VOY_IMPORT"].ToString();
                    string voy_export = myrow["VOY_EXPORT"].ToString();
                    string rec_time = System.DateTime.Now.ToString();
                    string eta = myrow["ETA"].ToString();
                    string etb = myrow["ETB"].ToString();
                    string etd = myrow["ETD"].ToString();
                    string closing_time = myrow["CLOSING_TIME"].ToString();
                    string ata = myrow["ATA"].ToString();
                    string atb = myrow["ATB"].ToString();
                    string atd = myrow["ATD"].ToString();
                    string sqlUpdate = "UPDATE  VOYAGE SET " +
                        "VES_ID = " + ves_id + "," +
                        "VOY_IMPORT =" + voy_import + "," +
                        "VOY_EXPORT =" + voy_export + "," +
                        "REC_TIME =" + ToDate(rec_time) + "," +
                        "ETA=" + ToDate(eta) + "," +
                        "ETB=" + ToDate(etb) + "," +
                        "ETD=" + ToDate(etd) + "," +
                        "CLOSING_TIME=" + ToDate(closing_time) + "," +
                        "ATA=" + ToDate(ata) + "," +
                        "ATB=" + ToDate(atb) + "," +
                        "ATD =" + ToDate(atd) +
                        " WHERE VES_ID = " + myrow["VES_ID"];
                    MyConnection = new DatabaseConnection("UPDATE", sqlUpdate);
                }
            }
            MessageBox.Show("保存成功！", "提示");
        }

        /// <summary>
        /// SQ语句时间转换
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string ToDate(string date)
        {
            return "to_date('" + date + "', 'YYYY/MM/DD HH24:MI:SS')";
        }
        /// <summary>
        /// 过滤
        /// </summary>
        /// <param name="term"></param>
        private void SetFilter(string term)
        {
            string key = "";
            for (int i = 0; i < MyConnection.DT_voyage.Columns.Count; i++)
            {

                key += "Convert(" + MyConnection.DT_voyage.Columns[i].ColumnName + ", 'System.String')  like '%" + term + "%' or ";
            }
            int index = key.LastIndexOf("or");
            if (index >= 0)
            {
                key = key.Substring(0, index);
            }
            MyConnection.DT_voyage.DefaultView.RowFilter = key;
        }
    }
}
