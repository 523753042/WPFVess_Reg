﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPFVess_Reg.ViewModel
{
    public class BaseCommand : ICommand
    {
        //private int index;
        private Action mAction;
        // private Action<int> myAction;
        private bool canExecute = false;
        public BaseCommand(Action action)
        {
            this.mAction = action;
        }

        public event EventHandler CanExecuteChanged = (sender, e) => { };

        public bool CanExecute(object parameter)
        {
            return !canExecute;
        }

        public void Execute(object parameter)
        {
            canExecute = true;
            mAction();
            //   myAction(index);
        }
    }
}
